# 3088 Pi hat Project

The Raspberry Pi Remote Sensors HAT is a pHAT that allows you to have remote sensors that are connected through a Lora network, the HAT allows you to create a node on the network and be able to send data from connected sensors to a central location where you read the data and make decisions as you wish. The HAT has a LoRa module used to transmit the data between nodes to the LoRa gateway, it also has basic sensors that measure temperature, light, humidity and moisture there is an extension to add more sensors to the HAT. The HAT can be attached to a Raspberry Pi series board with 40 GPIO Pins. The HAT can be used in smart agriculture where a farmer needs to monitor key parameters of agriculture such as soil moisture, soil minerals, soil pH level, temperature etc. in order to improve crop productivity and resource use efficiency.

A list of components and prices:
Resisters - R10
Capacitors- R15
Op-amp - R20
LEDs - R25
Transistors- R70


